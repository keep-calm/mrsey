import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

import $ from 'jquery';

window.jQuery = $;
require('@fancyapps/fancybox/dist/jquery.fancybox');


// Apply a specific class to the html element for browser support of cookies.
if (window.navigator.cookieEnabled) {
  document.documentElement.className = document.documentElement.className.replace(
    'supports-no-cookies',
    'supports-cookies'
  );
}

$(function() {
  $('.pass-link').on('click', function(e){
    e.preventDefault();
    $('.overlay').fadeIn(350);
    $('#Login').fadeIn(350);
  });
  $('.overlay, .close-abs-box').on('click', function(){
    $('.login-box').fadeOut(350);
    $('.overlay').fadeOut(350);
    $('#Login').fadeOut(350);
  })
});
