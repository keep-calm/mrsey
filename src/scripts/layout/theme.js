import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';

import {focusHash, bindInPageLinks} from '@shopify/theme-a11y';
import { formatMoney } from '@shopify/theme-currency';

import $ from 'jquery';
import { addToCart, getProductJson, createSizeBox } from "../sections/product";

import scrollLock from 'scroll-lock';
//import 'slick-carousel';
import 'headroom.js-latest';

window.jQuery = $;
require('@fancyapps/fancybox/dist/jquery.fancybox');
require('../../assets/slick.min.js');



import Headroom from 'headroom.js-latest';


// Common a11y fixes
focusHash();
bindInPageLinks();

// Apply a specific class to the html element for browser support of cookies.
if (window.navigator.cookieEnabled) {
  document.documentElement.className = document.documentElement.className.replace(
    'supports-no-cookies',
    'supports-cookies'
  );
}

$(function() {

  //Headroom
  var myElement = document.querySelector('.header');
  var headroom = new Headroom(myElement, {
    tolerance: {
      down : 10,
      up : 20
    },
    offset : 50
  });
  headroom.init();

  //Menu
  var megaMenu = $('.mega-menu'),
      megaMenuH = megaMenu.outerHeight(),
      header = $('.header'),
      headerH = header.outerHeight(),
      windowW = $(window).width(),
      siteWrap = $('.site-wrapper'),
      megaLink = $('.mega-link > a'),
      overlay = $('.overlay'),
      headerElements = $('.header-elements'),
      mobSearch = $('.header-search'),
      closeLink = $('.close-menu'),
      mobMenuBtn = $('.menu-button'),
      mobMenu = $('.mobile-menu'),
      mobMenuH = mobMenu.outerHeight();

  function contentPadding(){
    header = $('.header'),
        headerH = header.outerHeight(),
        windowW = $(window).width();
    if(windowW > 640){ siteWrap.css({'padding-top' : headerH+'px'}); }
    else { siteWrap.css({'padding-top' : '0px'}); }
  }
  contentPadding();

  function megaMenuPosition() {
    megaMenuH = megaMenu.outerHeight();
    megaMenu.css({'top' : -megaMenuH+'px'});
  }
  megaMenuPosition();

  function mobileMenuPosition() {
    megaMenuH = megaMenu.outerHeight();
    mobMenu.css({'top' : -mobMenuH+'px'});
  }
  mobileMenuPosition();

  function closeMega() {
    closeLink.removeClass('menu-opened');
    megaLink.delay(250).queue(function(next){
      $(this).removeClass('active');
      mobMenuBtn.fadeIn(250);
      next();
    });
    mobSearch.removeClass('opened');
    megaMenu.delay(100).queue(function(next){
      $(this).removeClass('opened');
      next();
    });
    header.delay(250).queue(function(next){
      $(this).removeClass('menu-opened');
      headerElements.removeClass('menu-opened');
      overlay.fadeOut(250);
      scrollLock.enablePageScroll();
      next();
    });
  }

  function closeMobMenu() {
    closeLink.removeClass('menu-opened');
    megaLink.delay(250).queue(function(next){
      $(this).removeClass('active');
      mobMenuBtn.removeClass('active');
      next();
    });
    mobSearch.removeClass('opened');
    mobMenu.delay(100).queue(function(next){
      $(this).removeClass('opened');
      next();
    });
    header.delay(250).queue(function(next){
      $(this).removeClass('menu-opened');
      headerElements.removeClass('menu-opened');
      overlay.fadeOut(250);
      scrollLock.enablePageScroll();
      next();
    });
  }

  megaLink.on('click',function(event){
    event.preventDefault();
    if(!megaLink.hasClass('active')){
      megaLink.addClass('active');
      mobMenuBtn.fadeOut(250);
      scrollLock.disablePageScroll();
      header.addClass('menu-opened');
      headerElements.addClass('menu-opened');
      overlay.fadeIn(250);
      megaMenu.delay(100).queue(function(next){
        $(this).addClass('opened');
        next();
      });
      mobSearch.delay(250).queue(function(next){
        $(this).addClass('opened');
        closeLink.addClass('menu-opened');
        next();
      });
    }
    else {
      closeMega();
    }
  });

  //Mobile menu
  mobMenuBtn.on('click',function(event){
    event.preventDefault();
    if(!mobMenuBtn.hasClass('active')){
      megaLink.addClass('active');
      mobMenuBtn.addClass('active');
      scrollLock.disablePageScroll();
      header.addClass('menu-opened');
      headerElements.addClass('menu-opened');
      overlay.fadeIn(250);
      mobMenu.delay(100).queue(function(next){
        $(this).addClass('opened');
        next();
      });
      mobSearch.delay(250).queue(function(next){
        $(this).addClass('opened');
        closeLink.addClass('menu-opened');
        next();
      });
    }
    else {
      closeMobMenu();
    }
  });

  overlay.add(closeLink).add(mobMenuBtn).on('click',function(){
    if(megaMenu.hasClass('opened')){
      closeMega();
    }
    if(mobMenu.hasClass('opened')){
      closeMobMenu();
    }
  });

  //Image Slider
  var imageSlider =  $('.image-slider');
  imageSlider.each(function(){
    var thisSlider = $(this);
    thisSlider.slick({
      infinite: true,
      fade: true,
      cssEase: 'linear',
      arrows: false,
      dots: true
    });
  });

  //Collections Slider
  function colSliderSet(){
    var windowW = $(window).width(),
        rowW = $('.row').width(),
        sliderPadding = (windowW - rowW)/2,
        sliderWrap = $('.fc-slider-wrap');
    if(sliderPadding > 0 && windowW > 1024) {
      sliderWrap.css({
        'padding-left' : sliderPadding+'px'
      });
    }
  }
  colSliderSet();
  var colSlider =  $('.fc-slider');
  colSlider.each(function(){
    var thisSlider = $(this);
    thisSlider.slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      dots: false,
      responsive: [
        {
          breakpoint: 1161,
          settings: {
            arrows: false
          }
        },
        {
          breakpoint: 641,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  });
  function colSlideSize() {
    var slideImg = $('.fc-slide-img'),
        slideW = slideImg.width(),
        slideH = slideW*1.3;
    if(windowW > 1024){
      slideImg.css({
        'height' : slideH+'px'
      });
    }
  }
  colSlideSize();

  //Testimonials Slider
  $('.testimonials-slider').slick({
    dots: false,
    arrows: true,
    infinite: true
  });

  //****** Product Slider *******/

  //Play Video
  function playVideo(slideVideoID) {
    var videoForPlayID = $('#slide-video-'+slideVideoID);
    // videoForPlayID.get(0).addEventListener('ended', function () {
    //     console.log('STOPPED');
    // }, false);
    videoForPlayID.get(0).play();
    //console.log('PLAY');
  }

  // Pause Video
  function stopVideo(slideVideoID){
    var videoForStopID = $('#slide-video-'+slideVideoID);
    videoForStopID.get(0).pause();
    //console.log('PAUSED');
  }

  var productSlider = $('.product-main-slider'),
      productSliderTh = $('.product-th-slider');
  productSlider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    swipe: false,
    asNavFor: '.product-th-slider',
    responsive: [
      {
        breakpoint: 641,
        settings: {
          dots: true,
          swipe: true,
          autoplay: true,
          autoplaySpeed: 10000,
        }
      }
    ]
  });
  productSliderTh.slick({
    slidesToShow: 7,
    slidesToScroll: 1,
    asNavFor: '.product-main-slider',
    arrows: false,
    dots: false,
    focusOnSelect: true,
    vertical: true,
    verticalSwiping: true,
    centerMode: false
  });

  $('.product-section .prod-col .product-sliders-box .product-main-slider-box .product-main-slider .slick-dots > li:first-child > button').addClass('anim');


  productSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var nextSlideItem = productSlider.find('.slick-slide[data-slick-index="'+nextSlide+'"]');
    var currentSlideItem = productSlider.find('.slick-slide[data-slick-index="'+currentSlide+'"]');
    if(currentSlideItem.hasClass('video-slide')){
      var curSlideVideoID = currentSlideItem.find('.slide-video').data('video-id');
      stopVideo(curSlideVideoID);
    }
    if(nextSlideItem.hasClass('video-slide')){
      var slideVideoID = nextSlideItem.find('.slide-video').data('video-id');
      playVideo(slideVideoID);
    }
    var currentSlideDot = productSlider.find('.slick-dots > li > #slick-slide-control0'+currentSlide);
    var nextSlideDot = productSlider.find('.slick-dots > li > #slick-slide-control0'+nextSlide);
    currentSlideDot.removeClass('anim');
    nextSlideDot.addClass('anim');
  });


  //Fancybox
  $('[data-fancybox]').fancybox({
    //smallBtn: true,
    animationEffect: 'zoom-in-out',
    transitionEffect: 'zoom-in-out',
    transitionDuration: 500
  });

  //Other Products Slider
  $('.other-products-slider').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 641,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: false,
          arrows: false
        }
      }
    ]
  });

  //Additional Products Slider
  var additionalSlider = $('.additional-products-slider');
  additionalSlider.slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: true
  });


  //Video Slide Auto Height
  function slideVideoAH(){
    var bigSlideImageH = $('.product-image-wrap').outerHeight(),
        slideVideoWrap = $('.product-video-wrap');
    if(bigSlideImageH > 0 && slideVideoWrap.length > 0) {
      slideVideoWrap.css({'height' : bigSlideImageH+'px'});
    }
  }
  slideVideoAH();

  //Up Selling Product
  var upSelProduct = $('.up-selling-product-box');
  upSelProduct.on('click',function () {
    $(this).toggleClass('active');
  });

  //Additional Up Selling Product
  var addUpSelProduct = $('.additional-up-selling-box');
  addUpSelProduct.on('click',function () {
    $(this).toggleClass('active');
  });

  //Cross Selling Product
  var crossSelProduct = $('.cross-selling-product-box'),
      additionalSlider = $('.additional-products-slider');
  crossSelProduct.on('click',function () {
    $(this).toggleClass('active');
    $(this).next('.additional-products-box').slideToggle(350);
    if(!additionalSlider.hasClass('init')) {
      $('.additional-products-slider').addClass('init').slick('reinit');
    }
  });

  //Details description
  var detailLink = $('.full-title');
  detailLink.on('click',function(){
    $(this).toggleClass('active').next('.full-text').slideToggle(350);
  });

  //Product Cart Count
  $('.product-quantity').each(function() {
    var spinner = $(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    const countCurrentProduct = input.val();
    calcRowPrice(spinner, countCurrentProduct);
    checkBlockBtn(parseFloat(input.val()), btnDown);

    btnUp.click(function() {
      var oldValue = parseFloat(input.val()),
          newVal = 0;
      if (oldValue >= max) {
        newVal = oldValue;
      } else {
        newVal = oldValue + 1;
      }

      const currentBtnDown = $(this).siblings('.quantity-down');
      checkBlockBtn(newVal, currentBtnDown);

      spinner.find('input').attr('value', newVal);
      spinner.find('input').trigger("change");

      calcRowPrice(spinner, newVal);

      let idProduct = $(this).attr('data-id');
      updateCart(idProduct, newVal)

    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val()),
          newVal = 0;
      if (oldValue <= min) {
        newVal = oldValue;
      } else {
        newVal = oldValue - 1;
      }

      checkBlockBtn(newVal, $(this));

      spinner.find("input").attr('value', newVal);
      spinner.find("input").trigger("change");

      calcRowPrice(spinner, newVal);
      let idProduct = $(this).attr('data-id');
      updateCart(idProduct, newVal)
    });
  });

  function checkBlockBtn(val, el) {
    if (+val === 1) {
      el.addClass('block-button');
    } else {
      el.removeClass('block-button');
    }
  }

  function calcRowPrice(spinner, newVal) {
    const currentRow = $(spinner.closest('.cart-product-row')[0]).find('.new-price');
    const currentPrice = currentRow.attr('data-price');
    const newPrice = currentPrice * newVal;
    currentRow.attr('data-counted-price', newPrice);
    currentRow.text(formatMoney(newPrice));
    calcAllPrice();
  }

  function calcAllPrice() {
    let allPrice = 0;
    $('.product-prices-info').each(function () {
      allPrice += +($(this).find('.new-price').attr('data-counted-price'));
    });

    $('.total-sum').text(formatMoney(allPrice));
  }


  //Add Address
  $('.btn.add-address').on('click',function(){
    $(this).closest('.account-addresses-sec').find('.add-address-form').slideDown(350);
  });
  $('.btn.close-address-form').on('click', function () {
    $(this).closest('.account-addresses-sec').find('.add-address-form').slideUp(350);
  });

  //Edit Address
  $('.address-link.address-edit').on('click',function(){
    $(this).closest('.address-box').find('.edit-address-form').slideDown(350);
  });
  $('.btn.close-edit-address-form').on('click', function () {
    $(this).closest('.address-box').find('.edit-address-form').slideUp(350);
  });

  //Close Pop-Up
  $('.close-pop,.pop-overlay').on('click',function(){
    $('.pop-overlay').fadeOut(350);
    $('.pop-up-box').fadeOut(350);
  });

  $(window).resize(function() {
    contentPadding();
    megaMenuPosition();
    mobileMenuPosition();
    if(megaMenu.hasClass('opened')){
      closeMega();
    }
    if(mobMenu.hasClass('opened')){
      closeMobMenu();
    }
    colSliderSet();
    colSlideSize();
  });


  //reset - password
  $('#reset-password').on('click', function(){
    $('.reset-form-section').slideDown();
  });
  $('#close-reset-password').on('click', function(){
    $('.reset-form-section').slideUp();
    window.location.hash="";
  });
  // if recover
  if(window.location.hash) {
    var recover = window.location.hash.substr(1);
    if(recover == 'recover'){
      $('.reset-form-section').css("display", "block");
    }
  }

  // Check additional product on cart
  const productSelect = $('#product-size');
  const handleAdditionalProduct = 'glass-screen-protector';
  const btnAddCart = $('.js-add-cart');

  const appSellBox = $('.js-up-sale-box');
  $('.cart-product-row').each(function () {
    if ($(this).attr('data-screen-protector') === 'true') {
      appSellBox.hide();
    }
  });

  if(appSellBox.is(':visible')) {
    getProductJson(handleAdditionalProduct).then(res => {
      createSizeBox(productSelect, res.variants);
    })
  }

  btnAddCart.on('click', async function () {
    const idProduct = productSelect.find('option:selected').attr('data-upsell-id');

    if (idProduct) {
      await addToCart(idProduct).then(res => {
        window.location.reload();
      })
    }
  });


  function updateCart(idProduct, counter) {
      return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/cart/change.js',
            data: {
              id: idProduct,
              quantity: counter
            },
            success: () => {
              resolve();
            },
            error: (error) => {
              reject(error);
            }
        })
      });
  };


  // $( window ).on( "load", function() {
  //   $('#preloader').fadeOut(350,function(){
  //     $(this).remove();
  //   });
  // });

});
