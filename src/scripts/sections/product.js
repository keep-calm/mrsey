/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */

import { formatMoney } from '@shopify/theme-currency';
import { register } from '@shopify/theme-sections';
import { default as $ } from 'jquery';

const CurrentElement = $('.js-other-product.current');
const totalPriceBox = $('.js-total-price');
const comparePrice = $('.js-compare-price');
const upSellingProductBox = $('.js-up-selling');
const crossSellingProductBox = $('.js-cross-additional-products');
const topTotalPriceBox = $('.js-top-current-price');
const sizeSelectMain = $('.js-size-select-main');
const sizeSelectAdditional = $('.js-size-select-additional');
const btnAddCart = $('.js-add-to-card');

let totalPrice = totalPriceBox.text().trim();
let additionalHandle = null;

register('product', {
  async onLoad() {
    this.renderPrice(totalPrice);
    // Activate first element on page
    const startElement = $(CurrentElement);
    startElement.find('img').addClass('active-product');

    this.product = await getProductJson(
      startElement.attr('data-product-handle')
    );
    createSizeBox(sizeSelectMain, this.product.variants);
    this.checkUpdateAllProducts(upSellingProductBox);
    this.checkUpdateAllProducts(crossSellingProductBox);
    this.listenChangeProductsVariant();
  },
  
  listenChangeProductsVariant() {
    this.listenChangeSizeProduct();
    this.listenAdditionalProduct();
    this.addCart();
  },
  
  renderTopPrice(price, compare) {
    topTotalPriceBox.text(this.getFormatPrice(price));
    comparePrice.text(this.getFormatPrice(compare));
  },
  
  async addCart() {
    await btnAddCart.on('click', async () => {
      const idMainProduct = this.getCheckedVariantId;
      await this.addAdditionalProductsToCart(idMainProduct);
      await addToCart(idMainProduct).then(() => {
        setTimeout(() => {
          window.location = '/cart';
        }, 250);
      })
    })
  },
  
  
  getCheckedMainOption() {
    return sizeSelectMain.find('option:selected').attr('data-sizebox-option');
  },
  
  getCheckedAdditionalOption() {
    return sizeSelectAdditional.find('option:selected').attr('data-sizebox-option');
  },
  
  getCheckedVariantId() {
    return +sizeSelectMain.find('option:selected').attr('data-upsell-id');
  },
  
  getMainPrice() {
    return +sizeSelectMain.find('option:selected').attr('value');
  },


  listenChangeSizeProduct() {
    const self = this;
    sizeSelectMain.on('change', function () {
      let price = sizeSelectMain.find('option:selected').val();
      let comparePrice = sizeSelectMain.find('option:selected').attr('data-sizebox-compare');
      self.renderTopPrice(price, comparePrice);
      self.renderPrice();
      self.checkUpdateAllProducts(upSellingProductBox);
    })
  },

// Add additionalOption product
  listenAdditionalProduct() {
    const self = this;
    upSellingProductBox.on('click', function () {
      self.checkUpdateAllProducts(upSellingProductBox);
    });
  },
  
  checkUpdateAllProducts(el) {
    if (el.length) {
      let prices = 0;
      additionalHandle = el.attr('data-handle');
  
      setTimeout(() => {
        if (el.hasClass('active')) {
          prices += +el.attr('data-price-product');
        }
    
        this.renderPrice(this.getMainPrice() + prices);
      }, 100);
    }
  },
  
  renderPrice(price) {
    totalPriceBox.text(this.getFormatPrice(price));
  },
  
  getFormatPrice(price) {
    return formatMoney(price, theme.moneyFormat);
  },
  
  async addAdditionalProductsToCart() {
    const self = this;
    let sizeAdditionalProduct = null;
    const additionalProduct = getAdditionalProduct();
    
    const checkedMainOption = this.getCheckedMainOption();
    const checkedAdditionalOption = this.getCheckedAdditionalOption();
    
    if (checkedMainOption) {
      sizeAdditionalProduct = checkedMainOption;
    }
    
    if (checkedAdditionalOption) {
      sizeAdditionalProduct = checkedAdditionalOption;
    }
  
    // ADD TO CART ADDITIONAL PRODUCT
    $('.js-additional-products').each(function () {
      if ($(this).hasClass('active')) {
        additionalProduct.then(async (res) => {
          for (const additionalProduct of res.variants) {
            if (sizeAdditionalProduct === additionalProduct.option1) {
              await addToCart(additionalProduct.id);
            }
          }
        });
      }
    });
  },
});

export const addToCart =  async (id) => {
  return await new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: '/cart/add.js',
      data: { id },
      success: () => {
        resolve();
      },
      error: (error) => {
        reject(error);
      }
    })
  });
};

export const getAdditionalProduct = async () => {
  return await getProductJson(
    additionalHandle
  );
};

export const getProductJson = async (handle) => {
  return await fetch(`/products/${ handle }.js`).then((response) => {
    return response.json();
  });
};

export const createSizeBox = (el, variants) => {
  let htmlArr = [];
  
  for (const variant of variants) {
    htmlArr.push(`
          <option
            value="${ variant.price }"
            data-sizebox-option="${ variant.option1 }"
            data-sizebox-price="${ variant.price }"
            data-sizebox-compare="${ variant.compare_at_price }"
            data-upsell-id="${ variant.id }">
        ${ variant.public_title }</option>
        `);
  }
  
  el.html('');
  el.append(htmlArr);
};
