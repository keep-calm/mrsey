/**
 * Product-Bundle Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product-Bundle template.
 *
 * @namespace product-bundle
 */


import { formatMoney } from '@shopify/theme-currency';
import { register } from '@shopify/theme-sections';
import { default as $ } from 'jquery';

const CurrentElement = $('.js-other-product.current');
const comparePrice = $('.js-compare-price');
const additionalProducts = $('.js-additional-products');
const totalPriceBox = $('.js-total-price');
const topTotalPriceBox = $('.js-top-current-price');
const sizeSelectMain = $('.js-size-select-main');
const sizeSelectAdditional = $('.js-size-select-additional');
const btnAddCart = $('.js-add-to-card');

let totalPrice = totalPriceBox.attr('data-total-price');

let mainPrice = 0;
let additionalPrice = 0;

register('product-bundle', {
  async onLoad() {
    mainPrice = totalPrice;
    this.renderPrice();

    // Activate first element on page
    const startElement = $(CurrentElement);
    startElement.find('img').addClass('active-product');

    this.product = await this.getProductJson(
      startElement.attr('data-product-handle')
    );

  //  this.renderTopPrice(this.product.price);
    this.createSizeBox(sizeSelectMain, this.product.variants);
    this.calculateAdditional();
    this.listenChangeProductsVariant();
  },

  listenChangeProductsVariant() {
  //  this.listenChangeProduct(topProducts);
    this.listenAdditionalProduct();
    this.listenChangeAdditional();
    this.listenChangeSize();
    this.addCart();
  },

  listenChangeSize() {
      sizeSelectMain.on('change', () => {
      let price = sizeSelectMain.find('option:selected').val();
      let comparePrice = sizeSelectMain.find('option:selected').attr('data-sizebox-compare');
      this.renderTopPrice(price, comparePrice);
      mainPrice = price;
      this.renderPrice();
    });
  },

  renderTopPrice(price, compare) {
    topTotalPriceBox.text(this.getFormatPrice(price));
    comparePrice.text(this.getFormatPrice(compare));
  },
  async addCart() {
    const self = this;

    btnAddCart.on('click', () => {
      const idMainProduct = +sizeSelectMain.find('option:selected').attr('data-upsell-id');
      const sizeMainProduct = sizeSelectMain.find('option:selected').attr('data-sizebox-option');
      const handleNotSize = $('.js-additional-no-size').attr('data-handle');

      let additionalSeconsSize;
      this.addToCart(idMainProduct);

      if ($('.js-additional-no-size').hasClass('active')) {
        this.getProductJson(handleNotSize).then(async res => {
          for (const variant of res.variants) {
            if (variant.option1 == sizeMainProduct) {
              let firstAdditionalId = variant.id;
              await self.addToCart(firstAdditionalId);
            }
          }
        }).then(() => {
          setTimeout(() => {
            $('.js-additional-second').each(function () {
              if ($(this).hasClass('active')) {
                let secondAdditionalId = +sizeSelectAdditional.find('option:selected').attr('data-upsell-id');
                additionalSeconsSize = sizeSelectAdditional.find('option:selected').attr('data-sizebox-option');
                self.addToCart(secondAdditionalId);
              }
            });
          }, 400);
        }).then(() => {
          setTimeout(() => {
            if ($('.js-additional-third').hasClass('active')) {
              const handleThird = $('.js-additional-third').attr('data-handle');
              this.getProductJson(handleThird).then(res => {
                     for (const variant of res.variants) {
                  if (variant.option1 == additionalSeconsSize) {
                    let idThird = variant.id;
                    this.addToCart(idThird);
                  }
                }
              });
            }
          }, 400);
          setTimeout(function(){
            window.location = '/cart';
          }, 1000);
        })
      }else{
        setTimeout(function(){
          window.location = '/cart';
        }, 1000);
      }
    })
  },

  listenChangeProduct(el) {
    // Listening change top products
    el.each((index, el) => {
      $(el).on('click', async () => {
        const element = $(el);
        // Remove prev active element
        $('.active-product').removeClass('active-product');

        // Add class to new element
        if (!element.find('img').hasClass('active-product')) {
          element.find('img').addClass('active-product');
        }

        //Get current product handle
        const handle = element.attr('data-product-handle');

        // Add data for current product and create select for this product
        await this.getProductJson(handle).then((res) => {
          this.createSizeBox(sizeSelectMain, res.variants);

          mainPrice = res.price;

          this.renderTopPrice(res.price);
          this.renderPrice();
        });
      });
    });
  },


  listenChangeAdditional() {
    const self = this;

    $('.js-additional-second').on('click', function () {
      if (!$(this).hasClass('active')) {
        $('.js-additional-product-slider').find('.active').removeClass('active');
        $(this).addClass('active').click();
      }
    });

    $('.js-additional-main-second').on('click', function () {
      if ($(this).hasClass('active')) {
        $('.js-additional-second:first').click();
      } else {
        $('.js-additional-product-slider').find('.active').removeClass('active');
        $('.js-additional-third').removeClass('active');
      }
    });

    additionalProducts.each(function () {
      $(this).on('click', function () {
        if ($(this).hasClass('active') && !$(this).hasClass('js-additional-no-size') ) {
          const handle = $(this).attr('data-handle');
          self.getProductJson(handle).then(res => {
            self.createSizeBox(sizeSelectAdditional, res.variants);
          });
        }

        self.calculateAdditional();
      });
    });
  },

// Add additionalOption product
  listenAdditionalProduct() {
    const self = this;

    additionalProducts.each(function () {
      $(this).on('click', function () {
        self.calculateAdditional();
      });
    });
  },

  calculateAdditional() {
    const self = this;
    additionalPrice = 0;

    additionalProducts.each(function () {
      const price = +$(this).attr('data-price-product');

      if ($(this).hasClass('active')) {
        additionalPrice += price;
      }

      self.renderPrice();
    });
  },

  createSizeBox(el, variants) {
    let htmlArr = [];

    for (const variant of variants) {
      htmlArr.push(`
          <option
            value="${ variant.price }"
            data-price="${ variant.price }"
            data-sizebox-option="${ variant.option1 }"
            data-sizebox-compare="${ variant.compare_at_price }"
            data-upsell-id="${ variant.id }">
        ${ variant.public_title }</option>
        `);
    }

    el.html('');
    el.append(htmlArr);
  },

  renderPrice() {
    const total = +(mainPrice) + +(additionalPrice);
    totalPriceBox.attr('data-total-price', total);
    totalPriceBox.text(this.getFormatPrice(total));
  },

  getFormatPrice(price) {
    return formatMoney(price, theme.moneyFormat);
  },

  getProductJson(handle) {
    return fetch(`/products/${ handle }.js`).then((response) => {
      return response.json();
    });
  },

  async addToCart(id) {
    return await new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/cart/add.js',
        data: { id },
        success: () => {
          resolve();
        },
        error: (error) => {
          reject(error);
        }
      })
    });
  }
});
