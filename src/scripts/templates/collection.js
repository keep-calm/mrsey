import {load} from '@shopify/theme-sections';
import {formatMoney} from '@shopify/theme-currency';
import * as Images from '@shopify/theme-images';


import $ from 'jquery';
import _ from 'lodash-es';


load('*');

$(function() {

   $('#product-sorting').on('change',function(){
      var Sort_by = $(this).val();
      $.ajax({
         type: 'GET',
         url: location.pathname +'?view=ajax&sort_by='+Sort_by,
         success: function(res){
           var products = $.parseJSON(res),
               SortedProducts = { products: products };
           $.each(SortedProducts.products, function(i, product) {
               product.priceFormat = formatMoney( product.price, theme.moneyFormat);
               product.compare_at_priceFormat = formatMoney( product.compare_at_price, theme.moneyFormat);
               product.ImageFormat = Images.getSizedImageUrl(product.featured_image, '800x');
           });

           RenderFilteredCollection(SortedProducts);
         },
         error: function(status){
            console.log(status);
         }
      });
       window.history.pushState('?sort_by='+Sort_by, null,'?sort_by='+Sort_by );

   });

   $('#product-model').on('change', function(){
       var size = $(this).val();
       console.log(size);
       $.ajax({
           type: 'GET',
           url: location.pathname +'/'+size+'/?view=ajax',
           success: function(res){
               var products = $.parseJSON(res),
                   SortedProducts = { products: products };
               $.each(SortedProducts.products, function(i, product) {
                   product.priceFormat = formatMoney( product.price, theme.moneyFormat);
                   product.compare_at_priceFormat = formatMoney( product.compare_at_price, theme.moneyFormat);
                   product.ImageFormat = Images.getSizedImageUrl(product.featured_image, '800x');
               });

               RenderFilteredCollection(SortedProducts);
           },
           error: function(status){
               console.log(status);
           }
       });
       window.history.pushState(null, document.title, '/'+size);
   });

   function RenderFilteredCollection(Products){
       var tmpl = _.template($('#sort-collection').html());
       $('#collection-content').html(tmpl(Products));
   }

});